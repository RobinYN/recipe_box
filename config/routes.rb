Rails.application.routes.draw do
devise_for :users
 resources :recipes
 get "/search", to: 'pages#search', :as => 'search_page'
 root "recipes#index"

end
