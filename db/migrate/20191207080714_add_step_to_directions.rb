class AddStepToDirections < ActiveRecord::Migration[5.2]
  def change
    add_column :directions, :step, :string
  end
end
